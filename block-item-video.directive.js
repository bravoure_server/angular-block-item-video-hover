(function () {

    'use strict';

    function blockItemVideo (PATH_CONFIG, $timeout, $analytics, $rootScope) {

        return {
            restrict: "E",
            transclude: false,
            replace: true,

            link: function (scope, element, attrs) {
                angular.element(element).hover(function(e){
                    console.log(e);
                    $('video', this).get(0).play();
                });


                angular.element('.block-item-video-hover__item').hover(function(e){
                    console.log('asa');
                });

                element.hover(function(e){
                    console.log('al');
                    $('video', this).get(0).play();
                });

                angular.element(document).find('.block-item-video-hover__item').hover(function(e){
                    console.log('asa');
                });

                /*

                console.log(element.find('.block-item-video-hover__item'));
                element.hover(function(){
                    console.log('a');
                    $(this).find('.block-item-video-hover__item').addClass('show');
                },function(){
                    console.log('b');
                    $(this).find('.block-item-video-hover__item').removeClass('show');
                });
*/

                var figure = $(".video").hover( hoverVideo, hideVideo );

                function hoverVideo(e) {
                    console.log('a');
                    $('video', this).get(0).play();
                }

                function hideVideo(e) {
                    $('video', this).get(0).pause();
                }
            },

            controller: function ($scope, $controller) {

                angular.extend(this, $controller('baseController', {
                    $scope: $scope
                }));

            },

            templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-block-item-video-hover/block-item-video.html'
        }
    }

    blockItemVideo.$inject = ['PATH_CONFIG', '$timeout', '$analytics', '$rootScope'];

    angular
        .module('bravoureAngularApp')
        .directive('blockItemVideo', blockItemVideo);

})();
