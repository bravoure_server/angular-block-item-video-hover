(function () {

    'use strict';

    function blockItemVideoHover (PATH_CONFIG, $timeout, $analytics, $rootScope) {

        return {
            restrict: "E",
            transclude: false,
            replace: true,

            link: function (scope, element, attrs) {

                element.hover(function(e){
                    $('video', this).get(0).play();
                }, function(e){
                    $('video', this).get(0).pause();
                });

            },

            controller: function ($scope, $controller) {

                angular.extend(this, $controller('baseController', {
                    $scope: $scope
                }));

            },

            templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-block-item-video-hover/block-item-video-hover.html'
        }
    }

    blockItemVideoHover.$inject = ['PATH_CONFIG', '$timeout', '$analytics', '$rootScope'];

    angular
        .module('bravoureAngularApp')
        .directive('blockItemVideoHover', blockItemVideoHover);

})();
