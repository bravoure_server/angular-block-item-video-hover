# Bravoure - Angular Block Item Video Hover 

## Directive: Code to be added:

        <block-item-video-hover></block-item-video-hover>

## Use

It can be added in the cms under the option of "blocks", and inserting the block-list-video-hover,
and inserting the content of each slide.


### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-block-item-video-hover": "1.0"
      }
    }

and the run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
